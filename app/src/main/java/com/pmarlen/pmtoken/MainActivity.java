package com.pmarlen.pmtoken;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pmarlen.model.GeneradorDeToken;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity  implements View.OnClickListener {
    EditText editText_frase;
    TextView textView_TOKEN;
    TextView textView_sysTime;
    Button button_generar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText_frase   = (EditText) findViewById(R.id.editText_frase);
        textView_TOKEN   = (TextView) findViewById(R.id.textView_TOKEN);
        textView_sysTime = (TextView) findViewById(R.id.textView_sysTime);
        button_generar   = (Button) findViewById(R.id.button_generar);

        button_generar.setOnClickListener(this);

        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,Spanned dest, int dstart, int dend) {
                Log.i("TRACKTOPELL","filter:->"+source+"<-");

                for (int i = start; i < end; i++) {
                    if (!Character.isDigit(source.charAt(i))) {
                        return "";
                    }
                }
                return null;
            }
        };
        Date sysDate =  new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long timeStamp = System.currentTimeMillis();

        textView_sysTime.setText("["+sdf.format(sysDate)+"] ("+timeStamp+")");

    }

    @Override
    public void onClick(View v) {
        if( v == button_generar){
            button_generar_onClick();
        }
    }

    private void button_generar_onClick(){
        Integer frase = null;
        try{
            frase = Integer.parseInt(editText_frase.getText().toString());
            if(frase > 100000 && frase < 999999 ){
                GeneradorDeToken gt = new GeneradorDeToken();

                String token = gt.getToken(editText_frase.getText().toString());
                Log.i("TRACKTOPELL","button_generar_onClick:frase="+frase);
                Log.i("TRACKTOPELL","button_generar_onClick:token="+token);
                textView_TOKEN.setText(token);
            } else{
                textView_TOKEN.setText("");
            }
        } catch(NumberFormatException nfe){
            editText_frase.setText("");
        }
    }
}